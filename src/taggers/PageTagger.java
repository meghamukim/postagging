package taggers;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class PageTagger {

	private static MaxentTagger tagger;

	/**
	 * User defined constructor for the class which takes in the model as a parameter
	 * to be used for initializing the tagger.
	 * @param model
	 */
	public PageTagger(String model) {
		tagger = new MaxentTagger(model);
	}

	/**
	 * This method takes in String and tags it for the Part of Speech
	 * using the Stanford POS Tagger (MaxentTagger)
	 * @param input
	 * @return
	 */
	private String tagText(String input){
		if(input == null || input.length() == 0)
			return "";
		
		StringBuilder output = new StringBuilder("");
		//The maximum number of words that should be sent to tagString method to avoid Memory Excpetions
		int maxLength = 250;
		StringTokenizer tok = new StringTokenizer(input, " ");

		if(tok.countTokens() < maxLength){
			output.append(tagger.tagString(input));
			return output.toString();
		}

		/*If the string is too big we try to split it into chunks of smaller size and send to the tagString method.
		This is because tagString takes up too much memory and ultimately we would end up with "Out of Memory Exception"*/
		StringBuilder outputStr = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = tok.nextToken();
			if (lineLen++  < maxLength) {
				outputStr.append(word+" ");
			} else {
				output.append(tagger.tagString(outputStr.toString()));
				outputStr.delete(0, outputStr.length());
				lineLen =0;
			}
		}
		return output.toString();
	}

	/**
	 * This method reads the HTML document from the input URL
	 * and returns the extracted text from HTML elements using the JSoup HTML Parser Library
	 * @param inputURL
	 * @return output
	 */
	private String getText(URL inputURL){
		if(inputURL == null || inputURL.toString().length() == 0)
			return "";

		String URLString = inputURL.toString();
		StringBuffer output = new StringBuffer("");

		try {
			Document doc = Jsoup.connect(URLString).get();

			//Access the body section and extract the text from the HTML elements present in there
			Elements textList = doc.body().getAllElements();

			//Iterate over the elements encountered in <body> and append the text from each element into the result String
			for(Element textNode: textList){
				output.append(textNode.text()+ " ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return output.toString();
	}

	public static void main(String[] args) {
		PageTagger posTagger = new PageTagger("trainedData/english-left3words-distsim.tagger");

		URL url;
		PrintWriter fileWriter = null;
		Scanner scanner = new Scanner(System.in);
		String inputURL = scanner.nextLine();
		try {
			url = new URL(inputURL);
			//Get the text from the HTML doc related to the URL
			String pText = posTagger.getText(url);
			//Tag the text returned from getText() method
			String tagged = posTagger.tagText(pText);

			///write tagged string to the file output.txt
			fileWriter = new PrintWriter("output.txt","utf-8");
			fileWriter.println(tagged);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			fileWriter.close();
			scanner.close();
		}
	}
}